package com.myntra.playarena;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlayArenaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlayArenaApplication.class, args);
	}

}
