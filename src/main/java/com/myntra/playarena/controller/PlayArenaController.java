package com.myntra.playarena.controller;

import com.myntra.playarena.request.UpdatePlayerRequest;
import com.myntra.playarena.response.PlayerInfoResponse;
import com.myntra.playarena.service.PlayArenaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(
    path = "/playArena",
    produces = MediaType.APPLICATION_JSON_VALUE,
    consumes = MediaType.APPLICATION_JSON_VALUE
)
public class PlayArenaController {
    @Autowired
    PlayArenaService playArenaService;

    @PostMapping("/register")
    public void registerGame(@RequestBody String name) {

    }

    @GetMapping("/getIndividualScore")
    @ResponseBody
    public ResponseEntity<Integer> getIndividualScore(@RequestParam Long startHour,
                                                     @RequestParam String playerId,
                                                     @RequestParam String arena,
                                                     @RequestParam String gameName) {
        return ResponseEntity.ok(playArenaService.getIndividualScore(startHour, playerId, arena, gameName));
    }

    @GetMapping("/getTopNWithScore")
    @ResponseBody
    public PlayerInfoResponse getTopNWithScore(@RequestParam Long N,
                                               @RequestParam Long startHour,
                                               @RequestParam String arena,
                                               @RequestParam String gameName) {
        return PlayerInfoResponse.builder().playerInfoList(playArenaService.getTopNWithScore(N, startHour, arena, gameName)).build();
    }

    @GetMapping("/getIndividualRank/")
    @ResponseBody
    public Integer getIndividualRank(@RequestParam String playerId,
                           @RequestParam String arena,
                           @RequestParam Long startHour,
                           @RequestParam String gameName) {
        return playArenaService.getIndividualRank(playerId, arena, startHour, gameName);
    }

    @PostMapping("/updatePlayerScore")
    @ResponseBody
    public void updatePlayerScore(@RequestBody UpdatePlayerRequest updatePlayerRequest) {
        playArenaService.updatePlayerScore(updatePlayerRequest);
    }
}