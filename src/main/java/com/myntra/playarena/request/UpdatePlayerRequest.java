package com.myntra.playarena.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdatePlayerRequest {
    String arena;
    String playerId;
    Long startHour;
    String gameName;
}
