package com.myntra.playarena.response;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class PlayerInfoResponse {
    List<PlayerInfo> playerInfoList;
}