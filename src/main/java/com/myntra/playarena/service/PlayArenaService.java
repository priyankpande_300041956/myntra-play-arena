package com.myntra.playarena.service;

import com.myntra.playarena.request.UpdatePlayerRequest;
import com.myntra.playarena.response.PlayerInfo;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
@Log4j2
public class PlayArenaService {
    @Autowired
    StringRedisTemplate stringRedisTemplate;

    public void updatePlayerScore(UpdatePlayerRequest updatePlayerRequest) {
        String key = updatePlayerRequest.getGameName() + "_" + updatePlayerRequest.getArena() + "_" + updatePlayerRequest.getStartHour();

        Integer score = stringRedisTemplate.opsForZSet().incrementScore(key, updatePlayerRequest.getPlayerId(), 1).intValue();
        if (score == 1) {
            stringRedisTemplate.expire(key, 24, TimeUnit.HOURS);
        }
    }

    public List<PlayerInfo> getTopNWithScore(Long N, Long startHour, String arena, String gameName) {

        String key = gameName + "_" + arena + "_" + startHour;

        final Set<ZSetOperations.TypedTuple<String>> typedTuples = stringRedisTemplate.opsForZSet().reverseRangeWithScores(key, 0, N - 1);

        log.info("typedTuples =" + typedTuples);

        List<PlayerInfo> playerInfos = typedTuples.stream()
            .map(typedTuple -> PlayerInfo.builder()
                .playerName(typedTuple.getValue())
                .score(typedTuple.getScore().intValue())
                .build()
            )
            .collect(Collectors.toList());

        return playerInfos;
    }

    public Integer getIndividualScore(Long startHour, String playerId, String arena, String gameName) {
        String key = gameName + "_" + arena + "_" + startHour;
        return stringRedisTemplate.opsForZSet().score(key, playerId).intValue();
    }

    public Integer getIndividualRank(String playerId, String arena, Long startHour, String gameName) {
        String key = gameName + "_" + arena + "_" + startHour;
        return stringRedisTemplate.opsForZSet().reverseRank(key, playerId).intValue();
    }
}