package com.example.OpsLeaderBoard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpsLeaderBoardApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpsLeaderBoardApplication.class, args);
	}

}
